var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var path = require('path');
var request = require('request');

global.port = process.env.PORT || 3100;


global.server = app.listen(global.port, (resp) => {
    console.log('server started on port: ' + global.port);
});
app.use(favicon(path.join(__dirname, "public", 'favicon.ico')));
app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    limit: '100mb',
    extended: true
}));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function (req, res) {
    res.sendFile(__dirname + "/public/views/index.html");
})

app.get('/preventive', function (req, res) {
    res.sendFile(__dirname + "/public/views/preventative.html");
})
app.get('/cosmetic', function (req, res) {
    res.sendFile(__dirname + "/public/views/cosmetic.html");
})
app.get('/emergency', function (req, res) {
    res.sendFile(__dirname + "/public/views/emergency.html");
})
app.get('/amenities', function (req, res) {
    res.sendFile(__dirname + "/public/views/amenities.html");
})
app.get('/periodontics', function (req, res) {
    res.sendFile(__dirname + "/public/views/periodontics.html");
})
app.get('/oral', function (req, res) {
    res.sendFile(__dirname + "/public/views/oral.html");
})
app.get('/restorative', function (req, res) {
    res.sendFile(__dirname + "/public/views/restorative.html");
})
app.get('/pediatric', function (req, res) {
    res.sendFile(__dirname + "/public/views/pediatric.html");
})


// app.get('/features', function (req, res) {
//     res.sendFile(__dirname + "/public/views/features.html");
// })

function startKeepAlive() {
    setInterval(function () {
        request('https://mai-dental.herokuapp.com/', function (err, res, body) {
            console.log('Good Morning :-)')
        })
    }, 20 * 60 * 1000);
}

startKeepAlive();